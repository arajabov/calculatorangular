import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Calculator';
  history = []

  constructor() {
    this.history.push("History: ")
  }
  
  addToHistory(entry) {
    this.history.push(entry)
    console.log(this.history)

  }
}
