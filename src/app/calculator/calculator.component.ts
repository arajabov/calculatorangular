import { Component, HostListener, OnInit, ElementRef, Renderer2, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {
  allowedKeys = ['0', "1", "2", "3", "4", "5", "6", "7", "8", "9", "*", "/", "+", "-", "%", "Enter", "Backspace", "Clear", "."]
  buttons: any = ["AC", "+/-", "%", "÷", "7", "8", "9", "x", "4", "5", "6", "-", "1", "2", "3", "+", "0", ".", "="]
  output: any = 0
  operations: any = ["%", "x", "÷" ,"+", "-"]
  operation: any
  operands: any = []
  change: boolean = false
  editable: boolean = true
  fontSize: number = 42
  history: any = []

  @Output() addToHistory: EventEmitter<any> = new EventEmitter();
  
  /* Keyboard Listener */ 
  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    // console.log(event.key)
    if (this.allowedKeys.includes(event.key)) {
      let key
      switch (event.key) {
        case "Enter":
          key = "="
          break
        case "*":
          key = "x"
          break
        case "/":
          key = "÷"
          break
        case "Clear":
          key = "AC"
          break
        case "Backspace":
          console.log(this.change)
          if (!this.editable) return
          this.changeFontSize("Backspace")
          this.output = this.output + ""
          if (this.output.length > 1 && this.output != "Infinity") {
            this.output = this.output.slice(0, this.output.length - 1)
            this.output = parseFloat(this.output)
          }
          else {
            this.output = parseFloat(this.output)
            this.clear()
          }
          return
        default:
          key = event.key
      }
      this.displaySign(key)
    }
  }

  /* LOGIC */ 
  displaySign(num) {
    this.changeFontSize()
    if (!this.operation) {
      this.operation = []
    }
    if (this.change) {
      this.output = 0
      this.change = false
      this.editable = true
    }
    if (this.operations.includes(num)) {
      this.getOperation(num)
      return
    }
    switch(num) {
      case "AC": 
        this.clear()
        return
      case "=":
        if (this.operation == []) return
        this.getResult()
        return
      case "+/-":
        this.reverseSign()
        return
    }
    console.log(this.operation, this.operands)
    if (num == ".") {
      this.output = this.output + ""
      if (this.output[this.output.length - 1] == ".") return
      this.output = this.output + num
    } else {
      this.output = parseFloat(this.output + num)
    }
  }

  changeFontSize(key = "") {
    this.output = this.output + ""
    console.log(this.output.length, this.fontSize, this.output.length > 7)
    if (this.output.length > 7) {
        if (key == "Backspace")
          this.fontSize = this.fontSize > 17 ? this.fontSize + 4 : this.fontSize 
        else
          this.fontSize = this.fontSize > 17 ? this.fontSize - 4 : this.fontSize 
    } else {
      this.fontSize = 42
    }
  }

  clear() {
    // debugger
    this.fontSize = 42
    this.operands = []
    this.output = 0
    this.operation = undefined
    this.editable =true
    console.log(this.operands)
  }

  saveNum() {
    if (!this.editable) return
    this.operands.push(this.output)
  }

  getOperation(num) {
    this.operation.push(num)
    this.saveNum()
    if(this.operands.length >= 2) this.getResult()
    this.change = true
  }

  reverseSign() {
    this.output = -this.output
  }

  getResult() {
    // debugger
    this.editable = true
    if (this.operands.length == 0) return 
    this.saveNum()
    console.log(this.operands)
    this.calculate()
    if (this.operands[0])
      this.output = this.operands[0]
    this.editable = false
    console.log(this.operands)
    this.addToHistory.emit(this.history)
    this.history = []
  }

  calculate() {
    let op1 : number = parseFloat(this.operands.shift())
    let op2 : number = parseFloat(this.operands.shift())

    if (this.operands != []) {
      switch (this.operation.shift()) {
        case "%":
          this.calculatePercentage(op1, op2)
          return
        case "x":
          this.multiple(op1, op2)
          break
        case "÷":
          this.divide(op1, op2)
          break
        case "+":
          this.add(op1, op2)
          break 
        case "-":
          this.substract(op1, op2)
          break
      }
    }
  }

  calculatePercentage(op1, op2) {
    this.operands[0] = op1 * op2 / 100
    this.history.push(op1 + " % " + op2 + " = " + this.operands[0])
  }

  multiple(op1, op2) {
    this.operands[0] = op1 * op2
    this.history.push(op1 + " x " + op2 + " = " + this.operands[0])
  }

  divide(op1, op2) {
    this.operands[0] = op1 / op2
    this.history.push(op1 + " ÷ " + op2 + " = " + this.operands[0])
  }

  add(op1, op2) {
    this.operands[0] = op1 + op2
    this.history.push(op1 + " + " + op2 + " = " + this.operands[0])
  }

  substract(op1, op2) {
    this.operands[0] = op1 - op2
    this.history.push(op1 + " - " + op2 + " = " + this.operands[0])
  }


  /* Draggable */ 
  mousemoveEvent: any;
    mouseupEvent: any;

    firstPopupX: number = 700;
    firstPopupY: number = 0;
    firstPopupZ: number = 3;
    xStep: number = 30;
    yStep: number = 30;
    zStep: number = 3;
    curX: number;
    curY: number;
    curZIndex: number;
    xStartElementPoint: number;
    yStartElementPoint: number;
    xStartMousePoint: number;
    yStartMousePoint: number;

    isMouseBtnOnPress: boolean;

    constructor(private elementRef: ElementRef,
      private renderer: Renderer2) {
      this.mouseup = this.unboundMouseup.bind(this);
      this.dragging = this.unboundDragging.bind(this);
  }

  mouseup: (event: any) => void;
  unboundMouseup(event: any) {
      // Remove listeners
      this.mousemoveEvent();
      this.mouseupEvent();
  }
  
  mousedown(event: any) {
    if (event.button === 0/*only left mouse click*/) {
        this.xStartElementPoint = this.curX;
        this.yStartElementPoint = this.curY;
        this.xStartMousePoint = event.pageX;
        this.yStartMousePoint = event.pageY;

        // if listeners exist, first Remove listeners
        if (this.mousemoveEvent)
            this.mousemoveEvent();
        if (this.mouseupEvent)
            this.mouseupEvent();

        this.mousemoveEvent = this.renderer.listen("document", "mousemove", this.dragging);
        this.mouseupEvent = this.renderer.listen("document", "mouseup", this.mouseup);
    }
  }
  dragging: (event: any) => void;
  unboundDragging(event: any) {
    this.curX = this.xStartElementPoint + (event.pageX - this.xStartMousePoint);
    this.curY = this.yStartElementPoint + (event.pageY - this.yStartMousePoint);
  }

  ngOnInit(): void {
    document.getSelection().removeAllRanges();
    this.setPos();
  }
  
  setPos() {
        this.curX = this.firstPopupX;
        this.curY = this.firstPopupY;
        this.curZIndex = this.firstPopupZ;
  }

}
